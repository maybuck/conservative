import React from 'react'
import './MostRecentTextImage.css'
import group4 from '../../img/group4.png'

function MostRecentTextImage(){
    return(
        <div className='MostRecentTextImage'>
            <img alt='group4' src={group4} />
            <div className='MostRecentTextImage_data'>
            <h2>Uber's Turbulent Week: Kalanick Out, New Twist In </h2>
            <span>7h</span>.
            </div>
        </div>
    )
}

export default MostRecentTextImage
