import React from "react";
import { Link } from "gatsby"
import './nav.css'


function navbar(){

    return(
        <div>

        <nav className="column is-12">
        <ul>
        <li><Link to="/">News</Link></li>
            <li><Link to="/">Videos</Link></li>
            <li><Link to="/">About Us</Link></li>
            <li><Link to="/">Contact Us</Link></li>
        </ul>
        </nav>
        </div>
    )
}

export default navbar