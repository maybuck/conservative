import React from 'react'
import './FeaturedStories.css'
import group2 from '../../img/group2.png'

function FeaturedStories(){

    return (
        <div className='featuredStories'>
            <img alt='featuredStories' src={group2} />
            <h2>Police to deploy more than 5,800 personnel for IMF-World</h2>
            <div className='featuredStories_usre_data'>
                <span>By</span>
                <span className='user'>John Doe</span>
                <span>OCT 15, 2021</span>
            </div>
        </div>
    )
}

export default FeaturedStories