import React from "react";
import './NewsLetterBlock.css';

function NewsLetterBlock(){


    return(
    <div className='newsletter_div'>
        <h2>Subscribe to our Newsletter</h2>
          <p>Get daily updates to all news reports, local and national coverage.</p>
          <form>
          <input type="email" name="newsletter" placeholder="Email" />
        <input type="submit" value="Submit" name='submit' />
          </form>  
    </div>
        
    )
}


export default  NewsLetterBlock