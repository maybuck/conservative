import React from 'react'
import './FeaturedTextImage.css'
import group3 from '../../img/group3.png'

function FeaturedTextImage(){

    return(
        <div className='FeaturedTextImage'>
            <div className='FeaturedTextImage_txt'>
                <h2>Smelter-grade alumina production reaches 2 million tons: Local firm</h2>
                <div className='FeaturedTextImage_usre_data'>
                <span>By</span>
                <span className='user'>John Doe</span>
                <span>OCT 15, 2021</span>
            </div>
             </div>
             <div className='FeaturedTextImage_img'><img alt='group3' src={group3} /> </div>
        </div>

    )
}

export default FeaturedTextImage