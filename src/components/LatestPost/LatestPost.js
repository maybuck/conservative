import React from "react";
import './LatestPost.css';
import post_img from '../../img/post_img.png';

function LatestPost(){

    return(
      
      <> 
      <div className='lastest_post'>
      <div style={{padding: '0'}}>
      <ul>
     <li>
     <img src={post_img} />
      <div className="latest_post_content">
      <h2>Lorem Ipsum is simply text dummy text of the printing and type setting</h2>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy </p>
      <div className="date_user">
        <span>By <b>Admin</b></span>
      <span>Oct 20, 2021</span>
      </div>
      </div>  
     </li>
     </ul>
     </div>
     </div>
     </> 
    )
}
export default LatestPost;