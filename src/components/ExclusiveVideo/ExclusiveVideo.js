import React from 'react'
import './ExclusiveVideo.css'
import Group1 from '../../img/Group1.png'

function ExclusiveVideo(){
    return(
        <div className='exclusive_ad'>
          <img alt='exclusive_ad' src={Group1} />
          <p>Here are 10 of the most populated cities in the world</p>
        </div>

    )
}
 export default ExclusiveVideo;