import React from 'react'
import PropTypes from 'prop-types'
import {graphql } from 'gatsby'
import Layout from '../components/Layout'
import NewsLetterBlock from '../components/NewsLetterBlock/NewsLetterBlock'
import twitter_ad from '../img/twitter_ad.png'
import logo from '../img/logo.png'
import ExclusiveVideo from '../components/ExclusiveVideo/ExclusiveVideo' 
import FeaturedStories from '../components/FeaturedStories/FeaturedStories'
import FeaturedTextImage from '../components/FeaturedTextImage/FeaturedTextImage'
import MostRecentTextImage from '../components/MostRecentTextImage/MostRecentTextImage'
import advertise from '../img/advertise.png'
import JustTitlePostDate from '../components/JustTitlePostDate/JustTitlePostDate'
import social_ad from '../img/social_ad.png'

export const IndexPageTemplate = ({
  image,
  title,
  heading,
  subheading,
  mainpitch,
  description,
  intro,
}) => (
  
    <div className="container">  
    <div className='column is-12 exclusive_video_container'>
      <div className='column is-3 column_th'>
        <div className='exclusive_video'>
        <h2 className='exclusive_video_h2'>Exclusive video</h2>
          <ExclusiveVideo />
          <div className='newsletter_block'>
          <NewsLetterBlock />
          </div>
          <div className='twitter_ad'>
            <img alt='twitter_ad' src={twitter_ad} />
          </div>
          <div className='exclusive_video_logo'>
            <img alt='logo' src={logo} />
          </div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
          <div className='exclusive_ad'><ExclusiveVideo /></div>
        </div>
      </div>
      <div className='column is-6 column_sixth'>
        <div className='featured_block'>
      <h2 className='h2'>Featured Stories</h2>
      <FeaturedStories />
      <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
        <div className='featured_block_data'>
        <FeaturedTextImage />
        </div>
      </div>
      </div>
      <div className='column is-3 column_third'>
        <div className='most_recent'>
        <h2 className='h2'>Most recent</h2>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <img alt='advertise' src={advertise} />
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <img alt='advertise' src={advertise} />
      </div>
      <div className='exclusive_video right_side_bottom'>
      <h2 className='exclusive_video_h2'>Trending</h2>
      <div className='twitter_ad'>
            <img alt='twitter_ad' src={twitter_ad} />
          </div>
      </div>
      <div className='exclusive_video right_side_bottom'>
      <h2 className='exclusive_video_h2'>Trending</h2>
      <div className='just_title'>
           <JustTitlePostDate />
          </div>
      <div className='just_title'>
           <JustTitlePostDate />
          </div>
          <img alt='advertise' src={advertise} style={{padding:'10px 0 20px 0'}}/> 
          <div className='just_title'>
           <JustTitlePostDate />
          </div>
          <div className='just_title'>
           <JustTitlePostDate />
          </div>
      <div className='just_title'>
           <JustTitlePostDate />
          </div>   
          <img alt='advertise' src={advertise}  style={{padding:'10px 0 20px 0'}}/>
          <div className='just_title'>
           <JustTitlePostDate />
          </div> 

      </div>
      </div>
    </div>
    <div className='column is-12 social_adver'><img alt='social_al' src={social_ad} /></div>
  </div>
)

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  heading: PropTypes.string,
  subheading: PropTypes.string,
  mainpitch: PropTypes.object,
  description: PropTypes.string,
  intro: PropTypes.shape({
    blurbs: PropTypes.array,
  }),
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        heading={frontmatter.heading}
        subheading={frontmatter.subheading}
        mainpitch={frontmatter.mainpitch}
        description={frontmatter.description}
        intro={frontmatter.intro}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        heading
        subheading
        mainpitch {
          title
          description
        }
        description
        intro {
          blurbs {
            image {
              childImageSharp {
                fluid(maxWidth: 240, quality: 64) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
            text
          }
          heading
          description
        }
      }
    }
  }
`
