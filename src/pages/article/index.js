import React from 'react';
import './article.css'
import Layout from '../../components/Layout'
import NewsArticle from '../../components/NewsArticle/NewsArticle'
import LatestPost from '../../components/LatestPost/LatestPost'
import google_ad from '../../img/google_ad.png'

function article (){

    return(
        <Layout>
            <div className='container'>
            <div class='article_0'>
            <NewsArticle />
            <div className='article_recent_post'>
            <LatestPost />
            <LatestPost />
            <LatestPost />
            </div>
            <div className='article_ad_im'><img src={google_ad} /></div>
            </div>
            </div>
        </Layout>

    )
}

export default article