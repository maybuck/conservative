import React from "react";
import Layout from "../../components/Layout"
import NewsLetterBlock from '../../components/NewsLetterBlock/NewsLetterBlock'
import JustTitlePostDate from '../../components/JustTitlePostDate/JustTitlePostDate'


const PrivacyPolicy = () => (
  <Layout>
   
    <div className="main-content">
        <div className="container">
  
           <div className="privacy_policy_right">
               <div className="column is-8">
                  <div className="PrivacyPolicy">
                     <h2>Privacy Policy</h2>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</p>
                     <h3 style={{marginTop:15}}>Collection of Information.</h3>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has beeny</p>
                     <ul>
                       <li>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</li>
                       <li>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</li>
                       <li>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</li>
                       <li>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</li>
                     </ul>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
                     <h3 style={{marginTop:15}}>Type of Information Collected by REDRIGHTNEWS.COM</h3>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing</p>
                     <ul>
                       <li>Lorem Ipsum has </li>
                       <li>Lorem Ipsum has been </li>
                       <li>Lorem Ipsum has been the </li>
                       <li>Lorem Ipsum has been the industry </li>
                     </ul>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing</p>
                     <h3 style={{marginTop:15}}>Sharing Personal Information with Third Parties</h3>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry</p>
                     <p>Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry s standard dummy Lorem Ipsum is simply dummy</p>
                  </div>
               </div>
               <div className="column is-4">
                  <div className='newsletter_row'>
                      <NewsLetterBlock />
                      <div className='right_side_bannerAd'>
                   
                        <div className='Top_News'>
                          <h2>TopNews</h2>
                        </div>
                        <JustTitlePostDate />
                        <JustTitlePostDate />
                        <JustTitlePostDate />
                        <JustTitlePostDate />
                 
                       </div>
                    </div>
               </div>
           </div>
        </div>
    </div>
  </Layout>
)

export default PrivacyPolicy
