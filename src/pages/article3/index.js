import React from 'react'
import Layout from "../../components/Layout"
import './article3.css'
import PrivacyPolicy from '../../components/PrivacyPolicy/PrivacyPolicy'
import advertise from '../../img/advertise.png'
import MostRecentTextImage from '../../components/MostRecentTextImage/MostRecentTextImage'
import twitter_ad from '../../img/twitter_ad.png'
import twitter_vote from '../../img/twitter_vote.png'
import ExclusiveVideo from '../../components/ExclusiveVideo/ExclusiveVideo'
import LatestPost from '../../components/LatestPost/LatestPost'

const Article3Page = () => (

    <Layout>
        <div className="container">
        <div className='column is-12 Article3Page' style={{padding: '0px'}}>
        <div className='is-9 Article3Page_col3'>
        <h3>Privacy Policy</h3>
        <PrivacyPolicy />
        </div>
        <div className='column is-3 column_third'>
        <div className='most_recent'>
        <h2 className='h2'>Most recent</h2>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <img alt='advertise' src={advertise} />
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <div className='most_recent_data'><MostRecentTextImage /></div>
        <img alt='advertise' src={advertise} />
        </div>
        <div className='sidebar_ad'><img src={twitter_vote}/></div>
        <div className='exclusive_video right_side_bottom'>
      <h2 className='exclusive_video_h2'>Trending</h2>
      <div className='twitter_ad'>
            <img alt='twitter_ad' src={twitter_ad} />
          </div>
      </div>
      <div className='exclusive_video right_side_bottom'>
      <h2 className='exclusive_video_h2'>Featured Video</h2>
      <div className='twitter_ad'>
            <ExclusiveVideo />
        </div>
        <div className='twitter_ad'>
            <ExclusiveVideo />
        </div>
        <div className='twitter_ad'>
            <ExclusiveVideo />
        </div>
      </div>
    
        </div>
        </div>

        <div className='article_recent_post privacy_post'>
            <LatestPost />
            <LatestPost />
            <LatestPost />
            </div>
        </div>
    </Layout>
) 

export default  Article3Page