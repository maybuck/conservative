import React from 'react'
import Layout from '../../components/Layout'
import './article2.css'
import twitter_vote from '../../img/twitter_vote.png'
import NewsArticle from '../../components/NewsArticle/NewsArticle'
import slideshow from '../../img/slideshow.png'

function article2(){
    return(
        <Layout>
            <div className='container'>
           <div className='column is-12 article2'>
               <div className='column is-3 article2_ad'>
                   <img src={twitter_vote} />
                   <img src={twitter_vote} />
                   <img src={twitter_vote} />
                </div>
                <div className='column is-6 article1_page_left_panel'>
                    <NewsArticle />
                </div>
                <div className='column is-3 article2_ad'>
                <img src={twitter_vote} />
                <img src={twitter_vote} />
                <img src={twitter_vote} />
                </div>   
           </div>
           <div className='article2_ad_bottom'><img src={slideshow} /></div>
           </div>
        </Layout>

    )
}

export default article2
