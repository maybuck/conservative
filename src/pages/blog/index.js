import React from 'react'

import Layout from '../../components/Layout'
import JustTitlePostDate from '../../components/JustTitlePostDate/JustTitlePostDate'
import NewsLetterBlock from '../../components/NewsLetterBlock/NewsLetterBlock'
import './blog.css'
import google_ad from '../../img/google_ad.png'
import coment from '../../img/coment.png'




export default class BlogIndexPage extends React.Component {
  render() {
    return (
      <Layout>
        <div className='container'>
         <div className='trending_truth_row'>
         <div className='column is-8 trending_truth_left_panel'>  
      <div className='related_post_title'>
      <div class="Top_News"><h2>Related Posts</h2></div>
      <div className='cv'>

      </div>
      </div>
      <img alt='google_ad' src={google_ad}  style={{width:'100%'}}/>
      <img alt='coment' src={coment}  style={{width:'100%', margin: '30px 0 40px 0'}}/>
         </div>
         <div className='column is-4 trending_truth_right_panel'>
         <div class="Top_News"><h2>TopNews</h2></div> 
         <div className='right_side_bannerAd'>
        <JustTitlePostDate />
        <JustTitlePostDate />

        <JustTitlePostDate />
        <JustTitlePostDate />
        <NewsLetterBlock />
     
        </div>
         </div>
         </div>
         </div>
      </Layout>
    )
  }
}
